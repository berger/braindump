#include "DatabaseInterface.h"

#include <QDebug>
#include <QDir>
#include <QStandardPaths>

#include <Cyqlops/DB/SQLite/Database.h>

#include "db/Database.h"

using namespace braindump;

QString DatabaseInterface::s_database_filename;

DatabaseInterface::DatabaseInterface(QObject* parent) : QObject(parent)
{
  QString db_filename = s_database_filename.isEmpty() ? QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/braindump.db" : s_database_filename;
  
  QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
  
  m_sqlite_database = new Cyqlops::DB::SQLite::Database;
  m_sqlite_database->setFilename(db_filename);
  m_sqlite_database->open();
  m_database = new db::Database(m_sqlite_database);

  m_root = db::Node::byType(m_database, db::Node::Type::Root).first();
  
  if(not m_root.isValid())
  {
    m_root = m_database->createNode(db::Node::Type::Root);
  }
}

void DatabaseInterface::setDatabaseFilename(const QString& _filename)
{
  s_database_filename = _filename;
}

