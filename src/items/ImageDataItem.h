#ifndef _IMAGEDATAITEM_H_
#define _IMAGEDATAITEM_H_

#include <QImage>
#include <QQuickItem>

class QSGTexture;

namespace braindump
{
  namespace db
  {
    class Data;
  }
  namespace items
  {
    class ImageDataItem : public QQuickItem
    {
      Q_OBJECT
      Q_PROPERTY(braindump::db::Data* data READ data WRITE setData NOTIFY dataChanged)
      Q_PROPERTY(qreal imageWidth READ imageWidth NOTIFY dataChanged)
      Q_PROPERTY(qreal imageHeight READ imageHeight NOTIFY dataChanged)
    public:
      ImageDataItem(QQuickItem* _parent = 0);
      virtual ~ImageDataItem();
    public:
      braindump::db::Data* data() const;
      void setData(const braindump::db::Data* _data);
      qreal imageWidth() const;
      qreal imageHeight() const;
      Q_INVOKABLE bool loadImage(braindump::db::Data* _data, const QUrl& _url);
    signals:
      void dataChanged();
    protected:
      virtual QSGNode* updatePaintNode(QSGNode* _oldNode, UpdatePaintNodeData* _upnd);
    private:
      struct Private;
      Private* const d;
    };
  }
}

#endif

