import QtQuick 2.0

import braindump.db 1.0 as BDB

Loader
{
  id: root
  property variant node
  
  onNodeChanged:
  {
    var newsource = ""
    switch(node.type + 0)
    {
      case BDB.Node.Root:
        newsource = "RootNode.qml"
        break;
      case BDB.Node.Text:
        newsource = "TextNode.qml"
        break;
      case BDB.Node.Url:
        newsource = "UrlNode.qml"
        break;
      case BDB.Node.Image:
        newsource = "ImageNode.qml"
        break;
      default:
        console.log("Unsupported node type: " + node.type, BDB.Node.Text, node.type == BDB.Node.Text, node.type === BDB.Node.Text, 1 === BDB.Node.Text, node.type === 1)
        break;
    }
    newsource = Qt.resolvedUrl(newsource)

    if(newsource != root.source)
    {
      root.source = newsource
    } else if(root.item) {
      root.item.node = root.node
    }
  }
  onLoaded:
  {
    root.item.node = root.node
  }
}
