import QtQuick 2.0
import QtQuick.Controls 2.2

import braindump 1.0
import braindump.Components 1.0
import braindump.db 1.0 as BDB

ScrollView
{
  id: root
  
  property DatabaseInterface databaseInterface
  property variant node
  
  ScrollBar.horizontal.interactive: true
  ScrollBar.vertical.interactive: true
  contentWidth: container.width
  contentHeight: container.height
  Item
  {
    id: container
    width: container_2.width + container_2.x
    height: container_2.height + container_2.y
    Item
    {
      id: container_2
      width: graph_view_row.width
      height: graph_view_row.height
      x: Math.max(0, root.width * 0.5 - (0.5 * centralNodeView.width + centralNodeView.x))
      y: Math.max(0, root.height * 0.5 - (0.5 * centralNodeView.height + centralNodeView.y))
      Row
      {
        id: graph_view_row
        spacing: 100
        Column
        {
          id: parentNodesColumn
          Repeater
          {
            id: parentNodes
            model: root.node.parentNodes()
            Row
            {
              Button
              {
                text: "<-"
                onClicked: root.node = modelData
              }
              Node
              {
                node: modelData
              }
            }
          }
        }
        Node
        {
          id: centralNodeView
          y: 0.5 * Math.max(parentNodesColumn.height, childrenNodesColumn.height) - 0.5 * centralNodeView.height
          node: root.node
        }
        Column
        {
          id: childrenNodesColumn
          Repeater
          {
            id: childrenNodes
            model: root.node.childNodes()
            Row
            {
              Node
              {
                node: modelData
              }
              Button
              {
                text: "->"
                onClicked: root.node = modelData
              }
            }
          }
          Row {
            id: newNode
            spacing: 5
            Button
            {
              text: "new text"
              onClicked: {
                root.node.createNewChild(BDB.Node.Text)
                childrenNodes.model = Qt.binding(function() { return root.node.childNodes() })
              }
            }
            Button
            {
              text: "new image"
              onClicked: {
                root.node.createNewChild(BDB.Node.Image)
                childrenNodes.model = Qt.binding(function() { return root.node.childNodes() })
              }
            }
            Button
            {
              text: "new url"
              onClicked: {
                root.node.createNewChild(BDB.Node.Url)
                childrenNodes.model = Qt.binding(function() { return root.node.childNodes() })
              }
            }
            Button
            {
              text: "new drawing"
            }
          }
        }
      }
      Component
      {
        id: connectionView
        Canvas
        {
          onWidthChanged: requestPaint()
          onHeightChanged: requestPaint()
          property real leftX: 0
          property real leftY
          property real rightX: width
          property real rightY
          property real middleX
          
          onPaint: {
            var ctx = getContext("2d");
            
            ctx.strokeStyle = "black"
            ctx.lineWidth = 1.0

            ctx.beginPath();
            ctx.moveTo(leftX, leftY)
            ctx.bezierCurveTo(leftX + 30, leftY, rightX - 30, rightY, rightX, rightY)
            ctx.stroke()
          }
        }
      }
      Repeater
      {
        model: parentNodes.model
        Loader
        {
          sourceComponent: connectionView
          property int index_index: index
          onLoaded:
          {
            f(index)
          }
          Connections
          {
            target: parentNodes
            onItemAdded:
            {
              f(index)
            }
          }
          function f(_index)
          {
            if(index == _index)
            {
              if(index == index_index)
              {
                item.x      = Qt.binding(function() { return parentNodes.itemAt(index).x + parentNodes.itemAt(index).width + 5 })
                item.y      = 0
                item.width  = Qt.binding(function() { return centralNodeView.x - item.x - 10 })
                item.height = Qt.binding(function() { return graph_view_row.height })

                item.leftY = Qt.binding(function() {  return parentNodes.itemAt(index).y + 0.5 * parentNodes.itemAt(index).height })
                item.rightY  = Qt.binding(function() { return centralNodeView.y + 0.5 * centralNodeView.height })

                item.middleX = Qt.binding(function() { return item.width - 5 })
              }
            }
          }
        }
      }
      Repeater
      {
        model: childrenNodes.model
        Loader
        {
          sourceComponent: connectionView
          Connections
          {
            target: childrenNodes
            onItemAdded:
            {
              f(index)
            }
          }
          onLoaded:
          {
            f(index)
          }
          function f(_index)
          {
            if(index == _index)
            {
              item.x      = Qt.binding(function() { return centralNodeView.x + centralNodeView.width + 5 })
              item.y      = 0
              item.width  = Qt.binding(function() { return (childrenNodes.itemAt(index) ? childrenNodes.itemAt(index).x : 0) + childrenNodesColumn.x - item.x - 10 })
              item.height = Qt.binding(function() { return graph_view_row.height })
              
              item.leftY  = Qt.binding(function() { return centralNodeView.y + 0.5 * centralNodeView.height })
              item.rightY = Qt.binding(function() { return (childrenNodes.itemAt(index) ? childrenNodes.itemAt(index).y + 0.5 * childrenNodes.itemAt(index).height  : 0) })

              item.middleX = 5
            }
          }
        }
      }
      Loader
      {
        sourceComponent: connectionView
        onLoaded: {
          item.x      = Qt.binding(function() { return centralNodeView.x + centralNodeView.width + 5 })
          item.y      = 0
          item.width  = Qt.binding(function() { return newNode.x + childrenNodesColumn.x - item.x - 10 })
          item.height = Qt.binding(function() { return graph_view_row.height })
          
          item.leftY  = Qt.binding(function() { return centralNodeView.y + 0.5 * centralNodeView.height })
          item.rightY = Qt.binding(function() { return newNode.y + 0.5 * newNode.height })

          item.middleX = 5
        }
      }
    }
  }
}
