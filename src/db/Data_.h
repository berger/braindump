public:
  Q_INVOKABLE QString asText() const;
  Q_INVOKABLE void setText(const QString& _text);
  Q_INVOKABLE QVariantMap asObject() const;
  Q_INVOKABLE void setObject(const QVariantMap& _map);
