#include "db/Node_p.h"

#include "db/NodeRelation.h"

using namespace braindump::db;

QList<QObject*> Node::parentNodes() const
{
  QList<QObject*> pNs;
  
  QList<NodeRelation> nodes_relation =  NodeRelation::byChildNode(d->database, *this).exec();  
  for(NodeRelation nr : nodes_relation)
  {
    pNs.append(new Node(nr.parentNode()));
  }
  
  return pNs;
}

QList<QObject*> Node::childNodes() const
{
  QList<QObject*> cNs;
  
  QList<NodeRelation> nodes_relation =  NodeRelation::byParentNode(d->database, *this).exec();  
  for(NodeRelation nr : nodes_relation)
  {
    cNs.append(new Node(nr.childNode()));
  }
  
  return cNs;
}

QObject* Node::createNewChild(int _type)
{
  Node node = d->database->createNode((Node::Type)_type);
  d->database->createNodeRelation(node, *this);
  return new Node(node);
}

QObject* Node::latestData(Data::Type _type) const
{
  Data data = (Data::byNode(d->database, *this)
                and Data::byType(d->database, _type)
              ).orderByTimestamp(Sql::Order::DESC).first();
  return new Data(data);
}

QObject* Node::newDataPoint(Data::Type _type) const
{
  Data prev_data = (Data::byNode(d->database, *this)
                and Data::byType(d->database, _type)
              ).orderByTimestamp(Sql::Order::DESC).first();
  QByteArray array;
  if(prev_data.isValid())
  {
    array = prev_data.data();
  }
  Data data = d->database->createData(*this, _type, array, QDateTime::currentDateTime());
  return new Data(data);
}
